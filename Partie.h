//
// Created by Ilias on 29/03/2016.
//

#ifndef TP_NOTE_PARTIE_H
#define TP_NOTE_PARTIE_H


#include <vector>
#include "User.h"
#include "Grille.h"
#include "Retour.h"

class Partie {
private:
    vector<User*> joueurs;
    Grille* grille;
    vector<int> socket;
    unsigned long joueurActuel;

public:

    Partie() : grille(NULL), joueurActuel(0) {

    }


    virtual ~Partie() {
        if(grille != NULL)
            delete(grille);
        for(unsigned long i = 0; i < joueurs.size(); i++){
            delete(joueurs.at(i));
        }
    }

    int creerGrille(int taille);
    int ajoutUser(int socket);
    void mainPartie();
    int commandes(vector<string> commande);
    int partieGagnee();
};


#endif //TP_NOTE_PARTIE_H
