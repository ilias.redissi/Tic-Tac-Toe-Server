//
// Created by Ilias on 29/03/2016.
//

#include "Grille.h"

#include <sstream>
#include <stdlib.h>

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

Grille::Grille(int taille) : taille(taille){
    grille = new User**[taille];
    for(int i = 0; i < taille; i++)
        grille[i] = new User*[taille];
    for(int i = 0; i < taille; i++){
        for(int j = 0; j < taille; j++) {
            grille[i][j] = NULL;
        }
    }
}

int Grille::jouer(User* joueur, unsigned int x, unsigned int y){
    if(joueur == NULL)
        return -1;
    if(x < taille && y < taille) {
        if(grille[y][x] == NULL)
            grille[y][x] = joueur;
    } else
        return COORDONNEES_NON_VALIDES;
    return OK;
}

string Grille::afficher(){
    //Exemple de grille
   /* a	  b   c
    +---+---+---+
   1| X | O |	|
    +---+---+---+
   2| O |   | O |
    +---+---+---+
   3| X |   | X |
    +---+---+---+*/
    string grilleString = "";
    if(taille >= 10)
        grilleString += " ";
    for(int i = 0; i < taille; i++){
        char x = 'a' + i;
        grilleString += "   ";
        grilleString += x;
    }

    grilleString += "\n";
    for(int i = 0; i < taille; i++){
        grilleString += ' ';
        if(taille >= 10)
            grilleString += " ";
        for(int j = 0; j < taille; j++)
            grilleString += "+---";
        if(taille >= 10 && i < 9)
            grilleString += "+\n" + patch::to_string(i + 1) + " ";
        else
            grilleString += "+\n" + patch::to_string(i + 1);
        for(int j = 0; j < taille; j++){
            grilleString += "| ";
            if(grille[i][j] != NULL)
                grilleString += grille[i][j]->getForme();
            else
                grilleString += " ";
            grilleString += " ";
        }

        grilleString += "|\n";
    }
    grilleString += " ";
    if(taille >= 10)
        grilleString += " ";
    for(int i = 0; i < taille; i++)
        grilleString += "+---";
    grilleString += "+\n";
    return grilleString;
};