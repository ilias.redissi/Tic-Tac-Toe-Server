all : executable
executable : Grille.o Partie.o Parser.o User.o server.o
	g++ -o server Grille.o Partie.o Parser.o server.o
User.o : User.cpp User.h
	g++ -c User.cpp
Grille.o : Grille.cpp Grille.h User.h
	g++ -c Grille.cpp
Partie.o : Partie.h Partie.cpp Grille.h User.h
	g++ -c Partie.cpp
Parser.o : Parser.cpp Parser.h
	g++ -c Parser.cpp
server.o : server.cpp Partie.h Parser.h Grille.h User.h
	g++ -c server.cpp
clean :
	rm Grille.o Partie.o Parser.o server.o server core