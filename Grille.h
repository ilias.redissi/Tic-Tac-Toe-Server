//
// Created by Ilias on 29/03/2016.
//

#ifndef TP_NOTE_GRILLE_H
#define TP_NOTE_GRILLE_H


#include "User.h"
#include <string>
#include "Retour.h"

using namespace std;

class Grille {
public:
    Grille(int taille);
    int jouer(User* joueur, unsigned int x, unsigned int y);
    string afficher();

    int getTaille() const {
        return taille;
    }

    User* getCase(unsigned x, unsigned y){
        if(x < taille && y < taille)
            return grille[x][y];
        return NULL;
    }

private:
    int taille;
    User*** grille;
};


#endif //TP_NOTE_GRILLE_H
