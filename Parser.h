//
// Created by Ilias on 05/04/2016.
//

#ifndef TP_NOTE_PARSER_H
#define TP_NOTE_PARSER_H

#include <vector>
#include <string>

using namespace std;

class Parser {
public:
    static vector<string> parse(string s, string delimiter);
};


#endif //TP_NOTE_PARSER_H
