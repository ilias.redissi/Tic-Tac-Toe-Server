#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "User.h"
#include "Grille.h"
#include "Partie.h"
#include "Parser.h"

#define MYPORT 3490
#define BACKLOG 10

#define TAILLE_MINIMUM 3
#define NB_JOUEUR_MAX 2

int main(void) {
    int sockfd, client1_fd, client2_fd;  // sock_fd : point de connexion, new_fd : socket de transit
    int maxfd;
    struct sockaddr_in my_addr;    // adresse de transport de la socket cotÃ© serveur
    struct sockaddr_in their_addr; // adresse de transport de la socket cotÃ© client

    int yes = 1;
    Partie partie;

    // Créer point de connextion et attente
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Creation socket : ");
        exit(EXIT_FAILURE);
    }
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
        perror("Options socket : ");
        exit(EXIT_FAILURE);
    }

    // Configuration de l'adresse de transport
    my_addr.sin_family = AF_INET;         // type de la socket
    my_addr.sin_port = htons(MYPORT);     // port, converti en reseau
    my_addr.sin_addr.s_addr = INADDR_ANY; // adresse, devrait Ãªtre converti en reseau mais est egal Ã  0
    bzero(&(my_addr.sin_zero), 8);        // mise a zero du reste de la structure

    // Creation du point de connexion : on ajoute l'adresse de transport dans la socket
    if ((bind(sockfd, (struct sockaddr *) &my_addr, sizeof(struct sockaddr))) == -1) {
        perror("Creation point connexion");
        exit(EXIT_FAILURE);
    }

    // Attente sur le point de connexion
    if (listen(sockfd, BACKLOG) == -1) {
        perror("Listen socket : ");
        exit(EXIT_FAILURE);
    }

    // attendre deux clients
    printf("attente du premier client!\n");
    unsigned int sin_size = sizeof(struct sockaddr);
    if ((client1_fd = accept(sockfd, (struct sockaddr *) &their_addr, (socklen_t *) &sin_size)) == -1) {
        perror("Accept premier client : ");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    printf("premier client: connection depuis %s\n", inet_ntoa(their_addr.sin_addr));

    //Ajout du joueur dans la partie
    partie.ajoutUser(client1_fd);

    printf("attente du deuxieme client!\n");

    if ((client2_fd = accept(sockfd, (struct sockaddr *) &their_addr, (socklen_t *) &sin_size)) == -1) {
        perror("Accept second client : ");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    printf("second client: connection depuis %s\n", inet_ntoa(their_addr.sin_addr));

    //Ajout du joueur dans la partie
    partie.ajoutUser(client2_fd);

    printf("attente du troisième client!\n");

    if ((client2_fd = accept(sockfd, (struct sockaddr *) &their_addr, (socklen_t *) &sin_size)) == -1) {
        perror("Accept second client : ");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    printf("second client: connection depuis %s\n", inet_ntoa(their_addr.sin_addr));

    //Ajout du joueur dans la partie
    partie.ajoutUser(client2_fd);

    // Determination du max
    if (client1_fd < client2_fd)
        maxfd = client2_fd + 1;
    else
        maxfd = client1_fd + 1;

    printf("maxfd = %d\n", maxfd);

    //Démarrage de la partie
    partie.mainPartie();

    // Agir en consequence
    close(client1_fd);
    close(client2_fd);
    close(sockfd);

    return 0;
}