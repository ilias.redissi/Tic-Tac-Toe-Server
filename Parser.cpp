//
// Created by Ilias on 05/04/2016.
//

#include <algorithm>
#include "Parser.h"

/**
 * Sépare une chaîne de caractères s selon un délimiteur
 */
vector<string> Parser::parse(string s, string delimiter) {
    vector<string> v;
    size_t pos = 0;
    std::string token;

    //Suppression des fins de ligne
    s.erase(std::remove(s.begin(), s.end(), '\r'), s.end());
    s.erase(std::remove(s.begin(), s.end(), '\n'), s.end());
    s += delimiter;


    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        s.erase(0, pos + delimiter.length());
        v.push_back(token);
    }
    return v;
}

