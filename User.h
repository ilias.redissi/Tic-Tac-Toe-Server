//
// Created by Ilias on 29/03/2016.
//

#ifndef TP_NOTE_USER_H
#define TP_NOTE_USER_H


#include <string>

using namespace std;

class User {
private:
    char forme;
    unsigned long numero;
public:
    int play(char x, int y);

    User(unsigned long numero) {
        this->numero = numero;
    }


    char getForme() {
        switch (numero){
            case 0:
                return 'X';
            case 1:
                return 'O';
            default:
                return (char) ('1' + numero);
        }
    }
};


#endif //TP_NOTE_USER_H
