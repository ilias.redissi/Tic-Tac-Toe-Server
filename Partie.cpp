//
// Created by Ilias on 29/03/2016.
//

#include <sys/select.h>
#include <cstdio>
#include <zconf.h>
#include <cstdlib>
#include <sys/socket.h>
#include <asm/byteorder.h>
#include "Partie.h"
#include "Parser.h"

int Partie::creerGrille(int taille) {
    //Taille minimale de la grille de 3
    if(taille >= 3)
        this->grille = new Grille(taille);
    else
        return PARTIE_NON_COMMENCEE;
    return PARTIE_COMMENCEE;
}

/**
 * Ajout d'un joueur de la partie ainsi que le socket de celui-ci
 */
int Partie::ajoutUser(int socket) {
    joueurs.push_back(new User(joueurs.size()));
    this->socket.push_back(socket);
    return OK;
}

/**
 * Fonction principale de Partie
 */
void Partie::mainPartie(){
    int maxfd = 0;
    joueurActuel = 0;
    string msg;


    if(socket.size() > 0)
        maxfd = socket.at(socket.size() - 1) + 1;

    fd_set readfds;
    fd_set originalfds;
    FD_ZERO(&originalfds);
    FD_ZERO(&readfds);
    for(unsigned long i = 0; i < socket.size(); i++){
        FD_SET(socket.at(i), &originalfds);
    }

    //Attende de la création de la grille
    while(grille == NULL) {
        readfds = originalfds;
        msg = "Créez la grille :\n";
        if(send(socket.at(joueurActuel), msg.c_str(), msg.size(), 0) == -1)
            perror("Erreur lors de l'envoi");
        if (select(maxfd, &readfds, NULL, NULL, NULL) == -1) {
            perror("Pb select");
            for (unsigned long i = 0; i < socket.size(); i++) {
                close(socket.at(i));
            }
            exit(EXIT_FAILURE);
        }

        if (FD_ISSET(socket.at(joueurActuel), &readfds)) {
            char buffer[100] = "";
            if(recv(socket.at(joueurActuel), buffer, 100, 0) == -1)
                perror("Erreur lors de la lecture");
            msg = buffer;
            commandes(Parser::parse(msg, " "));
            continue;
        }
    }
    printf("Grille crée");

    //Envoi de la grille à tous les joueurs
    for (unsigned long i = 0; i < socket.size(); i++) {
        send(socket.at(i), grille->afficher().c_str(), grille->afficher().size(), 0);
    }

    //Indique au bon joueur de joueur
    msg = "C'est votre tour : \n";
    send(socket.at(joueurActuel), msg.c_str() , msg.size(), 0);

    //La partie continue tant qu'elle n'est pas terminée
    while(partieGagnee() != PARTIE_FINIE){

        //Attente d'un message d'un des joueurs
        readfds = originalfds;
        if (select(maxfd, &readfds, NULL, NULL, NULL) == -1) {
            perror("Pb select");
            for(unsigned long i = 0; i < socket.size(); i++){
                close(socket.at(i));
            }
            exit(EXIT_FAILURE);
        }

        msg = "";
        //Vérification de qui a envoyé le message
        for(unsigned long i = 0; i < socket.size(); i++){
            if(i == joueurActuel){
                //Si c'est le joueur qui devait jouer qui envoie le message
                if (FD_ISSET(socket.at(i), &readfds)) {
                    char buffer[100] = "";
                    //Lecture du message
                    if(recv(socket.at(joueurActuel), buffer, 100, 0) == -1)
                        perror("Erreur lors de la lecture");
                    msg = buffer;
                    //
                    if(commandes(Parser::parse(msg, " ")) == OK) {
                        for (unsigned long j = 0; j < socket.size(); j++) {
                            send(socket.at(j), grille->afficher().c_str(), grille->afficher().size(), 0);
                        }
                        joueurActuel = ++joueurActuel % joueurs.size();
                        msg = "C'est votre tour : \n";
                        send(socket.at(joueurActuel), msg.c_str() , msg.size(), 0);
                    } else {
                        msg = "Entrez une commande valide : \n";
                        send(socket.at(joueurActuel), msg.c_str() , msg.size(), 0);
                    }
                    continue;
                }
            } else {
                //Si le joueur ne devait pas jouer
                if (FD_ISSET(socket.at(i), &readfds)) {
                    char buffer[100] = "";
                    if(recv(socket.at(i), buffer, 100, 0) == -1)
                        perror("Erreur lors de la lecture");
                    msg = "Ce n'est pas votre tour\n";
                    send(socket.at(i), msg.c_str(), msg.size(), 0);
                }
            }
        }
    }

    printf("Partie terminée");
}

int Partie::commandes(vector<string> commande) {
    if(commande.size() > 0){
        //Commande CREATE x
        if(commande.size() >= 2 && commande.at(0) == "CREATE" && atoi(commande.at(1).c_str()) >= 3 && grille == NULL){
            printf("taille : %d\n", atoi(commande.at(1).c_str()));
            return creerGrille(atoi(commande.at(1).c_str()));
        }
        //Commande PLAY x y
        if(commande.size() >= 3 && commande.at(0) == "PLAY" && commande.at(1).size() == 1 && atoi(commande.at(2).c_str()) > 0 && grille != NULL){
            int x = -1;
            char let = commande.at(1).at(0);
            if(let >= 'a' && let <= 'z')
                x = let - 'a';
            else if(let >= 'A' && let <= 'Z')
                x = let - 'A';
            int y = atoi(commande.at(2).c_str()) - 1;
            printf("y : %d\n", y);
            return grille->jouer(joueurs.at(joueurActuel), (unsigned int) x, (unsigned int) y);
        }
    }
    return -1;
}

int Partie::partieGagnee() {
    int taille = grille->getTaille();
    bool rempli = true;
    for(unsigned int i = 0; i < taille; i++){
        for(unsigned int j = 0; j < taille - 2; j++){
            if(grille->getCase(i, j) != NULL &&
                    grille->getCase(i, j) == grille->getCase(i, j + 1) && grille->getCase(i, j + 1) == grille->getCase(i, j + 2))
                return PARTIE_FINIE;
            if(grille->getCase(j ,i) != NULL &&
                    grille->getCase(j, i) == grille->getCase(j + 1, i) && grille->getCase(j+ 1, i) == grille->getCase(j + 2, i))
                return PARTIE_FINIE;
            if(grille->getCase(i ,j) == NULL)
                rempli = false;
        }
    }
    if(rempli)
        return PARTIE_FINIE;
    for(unsigned int j = 0; j < taille - 2; j++){
        if(grille->getCase(j, j) != NULL &&
                grille->getCase(j, j) == grille->getCase(j + 1, j + 1) && grille->getCase(j + 1, j + 1) == grille->getCase(j + 2, j + 2))
            return PARTIE_FINIE;
        if(grille->getCase(j, taille - j - 1) != NULL &&
                grille->getCase(j, taille - j - 1) == grille->getCase(j + 1, taille - j - 2) && grille->getCase(j+ 1, taille - j - 2) == grille->getCase(j + 2, taille - j  - 3))
            return PARTIE_FINIE;
    }
    return PARTIE_EN_COURS;
}














